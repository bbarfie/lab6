#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card deck[52];
   for (int i = 0; i < 52; i++)
   {
	   int cardz = i % 13;
	   deck[i].value = cardz + 2;
	   if(i < 13)
	   {
		   deck[i].suit = SPADES;
	   }
	   else if(i < 26) {
		   deck[i].suit = HEARTS;
	   }
	   else if(i < 39) {
		   deck[i].suit = DIAMONDS;
	   }
	   else {
		   deck[i].suit = CLUBS;
	   }
	}

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	
	random_shuffle(&deck[0], &deck[52], myrandom);
	
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
	Card handofCards[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
	sort(&handofCards[0], &handofCards[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
	string suitName;
	string card;
	for (int i = 0; i < 5; i++) {
		suitName = get_suit_code(handofCards[i]); //get suitname from function that interprets the code
		if(handofCards[i].value > 10) {
			card = get_card_name(handofCards[i]);
			cout << setw(10) << right << card << " of " << suitName << endl;
		}
		else { //the face cards
			cout << setw(10) << right << handofCards[i].value <<" of " << suitName << endl;
		}
	}


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  if (lhs.suit < rhs.suit) //sort left and right hand
  {
	return true;
  }
  else if (lhs.suit == rhs.suit)
 { //if the suits are the same
	  if(lhs.value < rhs.value) { // sort values
		  return true;
	  }
  }

  return false;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  if(c.value == 11) {
	  return "Jack";
  }
  else if(c.value == 12)  {
	  return "Queen";
  }
  else if(c.value == 13) {
	  return "King";
  }
  else if(c.value == 14) {
	  return "Ace";
  }
  else {
	  return "";
  }
}
